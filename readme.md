## Vtt-Vocabulary-Extractor (VVE)

- VttVocabularyExtractor is used for extract vocabulary from vtt file of youtube.

- Usage
  - build exe，`$ build.bat` 
  - execute，`$ vve.exe sample.vtt`
  - clean，`$ clean.bat`

## Todos

- commandline-api
  - download subtitle
  - query words
  - show zn-en、en-en、root、synonyms、antonym

- GUI
  - import txt

  - select word that user wants to memory
    - card list
    - show translate when hover
    - filter by frequency
  
  - show word information + pronounce
    - zn-en、en-en、root、synonyms、antonym

## Resource
  - [ECDICT，英漢詞典開源數據庫](https://github.com/skywind3000/ECDICT)
  - [typing-learner](https://github.com/tangshimin/typing-learner)