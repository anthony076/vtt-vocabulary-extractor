from argparse import ArgumentParser
from re import search
from logging import basicConfig, getLogger, INFO, DEBUG, debug, info, error

basicConfig(
  level=INFO,
  format="[%(lineno)s][%(funcName)s] %(message)s"
)

logger = getLogger(__name__)

class VttVocabularyExtractor:
  version = "0.1"

  def __init__(self, vtt: str) -> None:
    self._rawLines = []
    self._resultLines = []

    try: 
      with open(vtt) as f:
        self._rawLines:list[str] = f.readlines()
    except Exception as err:
      error(err)

  @property
  def rawLines(self):
    return self._rawLines
  
  @property
  def resultLines(self):
    return self._resultLines

  def filter_timestamp_line(self) -> 'VttVocabularyExtractor':

    for rawLine in self._rawLines:
      match = search("[\d]+", rawLine)

      debug(f"rawLine={rawLine}".strip())
      debug(f"match={match}")

      if not match:
        # 保留沒有數字的行
        self._resultLines.append(rawLine)
    
    return self

  def remove_lines(self) -> 'VttVocabularyExtractor':
    """
    - remove first 3 lines for youtuube-vtt
    - remove duplicated lines
    - remove empty string
    """
    if len(self._resultLines) == 0:
      logger.info("no content in file")
      return self
    
    # remove first 3 lines for youtuube-vtt
    self._resultLines = self._resultLines[3:]

    # remove duplicated lines
    self._resultLines = sorted(set(self._resultLines), key=self._resultLines.index)

    # remove empty string
    if "" in self._resultLines:
      self._resultLines.remove("")
    
    return self

  def write2file(self) -> 'VttVocabularyExtractor':
    with open("result.txt", mode='w', encoding="utf-8") as fw:
      fw.writelines(self._resultLines)
    
    return self

if __name__ == "__main__":
  print(f"VttVocabularyExtractor {VttVocabularyExtractor.version}")

  parser = ArgumentParser()
  parser.add_argument("file", help="specific vtt file", type=str)
  args = parser.parse_args()

  vve = VttVocabularyExtractor(args.file)
  vve \
    .filter_timestamp_line() \
    .remove_lines() \
    .write2file()

  print("Done !!")
